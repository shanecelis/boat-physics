﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
   I'M A BOAT, MOTHER *bleep*er. NEVER FORGET.
 */
public class BoatForces : MonoBehaviour {

  public Speed wind;
  public Speed water;

  public Transform rudder;
  public Transform sail;
  public Transform keel;

  public float sailArea = 1f;
  public float keelArea = 1f;
  public float rudderArea = 1f;

  public float k_air = 1f;
  public float k_water = 1f;

  public float rudderAbsMaximum = 45f;
  public float rudderAngle = 0f;

  public float rotateRudderDeltaAngle = 1f;
  public float rotateSailDeltaAngle = 1f;

  public Rigidbody body;

  void Start () { }

  void Update () {
    if (Input.GetKey(KeyCode.A))
      rudderAngle += rotateSailDeltaAngle;

    if (Input.GetKey(KeyCode.D))
      rudderAngle -= rotateSailDeltaAngle;

    rudderAngle = Mathf.Clamp(rudderAngle, -rudderAbsMaximum, rudderAbsMaximum);
    rudder.localRotation = Quaternion.AngleAxis(90f + rudderAngle, Vector3.up);

    if (Input.GetKey(KeyCode.Q))
      sail.Rotate(Vector3.up, rotateSailDeltaAngle);

    if (Input.GetKey(KeyCode.E))
      sail.Rotate(Vector3.up, -rotateSailDeltaAngle);

    if (Input.GetKey(KeyCode.W))
      sail.gameObject.SetActive(true);

    if (Input.GetKey(KeyCode.S))
      sail.gameObject.SetActive(false);

  }

  void FixedUpdate() {
    var v_boat = body.velocity;

    var v_wind = wind.speed * wind.transform.forward;
    var v_water = water.speed * water.transform.forward;
    var v_keel = body.GetPointVelocity(keel.position);
    var v_rudder = body.GetPointVelocity(rudder.position);
    var F_sail = k_air * sailArea * Vector3.Dot(sail.forward, v_wind - v_boat) * sail.forward;
    var F_keel = k_water * keelArea * Vector3.Dot(keel.forward, v_water - v_keel) * keel.forward;
    var F_rudder = k_water * rudderArea * Vector3.Dot(rudder.forward, v_water - v_rudder) * rudder.forward;

    body.AddForceAtPosition(F_rudder, rudder.transform.position);
    if (sail.gameObject.activeSelf)
      body.AddForceAtPosition(F_sail, sail.transform.position);
    body.AddForceAtPosition(F_keel, keel.transform.position);

  }
}
