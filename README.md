Boat Physics
============

I always wanted to model some boat physics.

## Requirements
- Unity 2017.3.0f3

## Optional
- buoyancy repo 

  Necessary if you want to run the advanced boat & buoyancy scene. (Not
  publicly available.)

## Parameters

- Velocity of water and wind

  This is represented as a transform where forward is the velocity direction, and
a `Speed` component holds its magnitude.

- Area of sail, keel, rudder
- Placement of sail, keel, rudder (not deathly important but does affect torques)

- Physics constants k\_air and k\_water represent the efficacy of water vs air
  propulsion. (All things being equal water on a board ought to push more than
  air on a sail, so $k\_water >> k\_air$.)

## Variables
- Boat velocity
- Rudder angle
- Sail angle
- Sail up or down

## User Interface

Steer the rudder with 'A' and 'D' keys. Angle the sail with 'Q' and 'E' keys.
Bring the sail up with 'W' and down with 'S'.

## Scenes

The project contains two scenes. The script of interest is `BoatForces` on the
'hull' object.

### Basic Boat

The hull can only rotate on the Y axis. Its gravity is turned off. In order to
let it rotate freely, you kind of need some kind of buoyancy to counteract the
wind pushing the sail down, thus the next scene.

### Advanced Boat & Buoyancy (AB&B)

The hull can rotate freely. Gravity is turned on, and the water plane is used to
determine the boat's buoyancy which is applied at the centroid which is under
the plane, thus you can get torque; this is what helps keep the boat right side
up when the sail pushes it over.

Again, you need the buoyancy repository in order to run this scene, otherwise
the boat will simply drop into the center of the earth due to gravity being on
and no buoyancy force.

## Suggested Improvements

- Change the wind and water direction automatically
- Add an anchor system
- Take the boat out to sea with a water cloth surface (in buoyancy repo)
- Model the sail being up or down with something richer than a boolean.

## Ideas for Future Work

This could make the boat part of a game much more interesting and challenging.
In some cases that'll be a not worthwhile distraction. In other cases maybe it'll help add 
depth to a game.

With this system a boat could capsize, which is an interesting failure state.

Since this is physics-based the boat can collide with other objects in
interesting ways.  For instance, a big shark or whale might knock the boat.

Considering the fishing angle, one could have the pull of a fish add another
force to the boat. Big fish might put you in jeopardy of being pulled into
things or capsizing.

The parameters of the boat can be used to create a lot of variety for
encouraging customization and substantive upgrades.

Raising and lowering the sail might be better modeled for game play purposes.
Suppose the sail is hard to set up, but easy to drop. That might provide for
some interesting game play choices since you don't have just a gas pedal to push down on.

Visually demonstrating that the sail is catching some wind might be nice.
Neither the wind nor the water direction can be seen in the current scenes.
